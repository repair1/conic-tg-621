# Conic TV Sports TG-621 - adding VBS output

## The Device

The Conic TG-621 is a Pong-style TV game with

- a built-in loudspeaker,
- a compartment for six 'C' cells to power it from
- a 3.5mm mono-audio-jack style connector for an external power supply (6V..9V)
- an RF output for TV channels 3 or 4 (European PAL norm).

It also comes with two 'paddle controllers' (potentiometers with a knob)
that are connected with 3.5mm mono-audio jacks at the front of the console.

![Conic TG-621, a Pong-style TV game](media/conic-2020-07-27-tg-621-w800.png)

## The Goal

The TV game console was designed for use with Television sets.

Therefore its main output is an RF-modulated signal on TV channel 3 or 4 (selectable with a switch on the underside of the device).

Problem with this classic 'terrestrial TV output' format is that

- with the move from terrestrial analog to DVB-T and to Internet-based TV, in the long term, new TV sets will drop support for it
- the extra modulation to RF and the demodulation inside the TV add noise to the output signal, reducing picture quality
- when using the device for events, tuning to the correct TV channel takes more time, and visitors can de-tune the TV easily (haha, how funny)

Therefore the goal was to replace the existing RF cable with an A/V cable that carries a 'VBS' signal with:

- Video
- Blanking
- Synchronization signals

(If we also had colour, we would want CVBS, but the Conic only has pure black and white.)

## Implementation Notes

### Construction

This TV game is built around a [General Instruments (GI) AY-3-8500 chip](http://pong-story.com/gi.htm).

The chip integrates all digital components necessary to produce a TV-suitable signal for a Pong-style game.

Interestingly, there are separate output pins for

- playfield and score
- ball
- left player (bat)
- right player (bat)
- synchronization

This makes it possible to change the brightness distribution - instead of a classic black background with playfield, score, ball and player bars in white, GI suggests in its [GIMINI catalog/datasheet](https://web.archive.org/web/20120316160455/http://www.pong-story.com/GIMINI1978.pdf) a circuit that e.g. uses gray for the background and switches one player display to black. This makes it easier to distinguish players in 'Handball' mode. There is also a companion chip - the AY-3-8515 - that takes the separate signals as input and adds colour to the output signal.

### Findings

- The console works well on a modern TV with terrestrial (analog) TV input.
- The TV signal has no audio component, the Conic comes with a separate built-in loudspeaker (8&ohm;, 0.2W) and with a separate on-off switch for it.
- There are [Chinese letters hand-written on the battery compartment](media/conic-battcomp-writing.jpg). A friendly co-worker translated them for me, they mean 'the picture is not shown correctly'. (A remark from a quality assurance tester at the factory?)
- The original power wiring had the colours wrong: red was GND, black was +9V power.
- The game selector switch has six positions, but the lower two ones are physically blocked by a metal strip, and the output pins are also not connected on the circuit board. The metal strip (together with a black ring around the slider) can sometimes block the switch to reach the lowest 'Practice' position, the switch is then between the lower two game modes, resulting in a field for two players (instead of Handball for one), where the left player has two bats, and the right player has three bats.
- A partial circuit trace revealed that all output signals of the chip are directly wired together:
  ![partial circuit of Conic TG-621](fritzing/conic-tg-621-schematic.png)

With all signals wired together, we cannot use one of the standard composite video mod circuits (like they are used for the Atari 2600) with two resistors and one transistor. These need separate Video and Sync signals.

Luckily, the signal that is routed from the chip (via an inductor and a rather long piece of wire) to the RF modulator part of the Conic looks already a lot like a VBS signal. The main problem is that it does not have the required amplitude - after the little inductor (see circuit diagram), the signal level is about 348mV.

But for [Composite Video](https://en.wikipedia.org/wiki/Composite_video) (C)VBS we need a signal level of about 1V (1073mV).

Tests with the output signal with several converters resulted in a video display, but synchronization was difficult, and the picture was very dark - a typical symptom of a signal that is too weak.

So we need to make it stronger. We need an amplifier for the VBS signal.

### VBS Amplifier

There is a number of amplifier circuits (cable video amplifier, etc.), but most of them are for 12V.

Somewhat closer to 6.2V are these 5V circuits:

- [Simple Video Amplifier](https://www.electronicsforu.com/electronics-projects/simple-video-amplifier)
- [Video Amplifier Circuit for Camera](http://circuitsector.blogspot.com/2010/05/video-amplifier-circuit-for-camera.html)

Problem: with 6.2V the circuit outputs 'high' all the time (output close to 6V). Clearly some component values need to be changed to establish a correct operating point.

My first idea was to model the circuit in e.g. KiCAD:  
![vbs-amp in KiCAD](kicad/vbs-amp-schematic.png)

And then trying to simulate it (KiCAD can do this, with ngspice). Unfortunately I got a couple of 'time step too small' errors - the simulation aborted because of some 'runaway' signals.

But I had an old (in computer terms: ancient) program called 'Electronics Work Bench' (EWB). It came with a book about [175 electronic experiments](https://www.amazon.de/Experimente-Elektronik-Design-Labor-Simulationssystem/dp/3772359906) to conduct within a simulator. I still have the installation files and the unlock code on CD. Great!

Unfortunately it does not work on Windows 10 / 64 bit. Looking after EWB, it has been taken over / been absorbed by [Multisim, and this piece of software has been taken over by National Instruments (NI)](https://www.ni.com/nl-nl/shop/electronic-test-instrumentation/application-software-for-electronic-test-and-instrumentation-category/what-is-multisim.html).

Multisim is a serious piece of software. Yes, the simulator core itself, SPICE, is free software. But there are (at least) two important items that differentiate the professional version from the free one:

1. SPICE Models - a set of parameters describing the exact characteristics of any component, e.g. a transistor or a diode. Professional producers like National Instruments work together with manufactureres and vendors to get verified and reliable data that accurately models commercial available real-world components.

2. Assistants - when I simulated the circuit with Multisim, the simulator stopped with 'time step too small'. Again! Hm. But then an 'Assistant' window asked if it should try to remedy the issue. To my surprise it then changed several parameters of the analog circuit simulation, until the issue was resolved. Then it took back bit by bit of the measures before the last change to leave the minimum set of changes to resolve the problem. Wow!

Luckily you can download and test the professional version of Multisim and try it for seven days. Many thanks, [NI](https://www.ni.com/)!

After I experimented with the circuit and made component value changes until the output looked good (on a simulated oscilloscope), this is the revised circuit diagram in Multisim:  
![vbs-amp in Multisim](multisim/vbs-amp-003-multisim-corr5.png)

One of the programs I also used to enter the circuit - even before KiCAD, because I could not download an installable version of KiCAD at first - is [Fritzing](https://fritzing.org/). (For this project, I tested several circuit drawing programs, but that would be worth an article opf its own.)

Here is the Fritzing version of the circuit diagram:  
![vbs-amp schematic in Fritzing](fritzing/video-amplifier-schematic.png)

Nice with Fritzing is that it you can also make a breadboard and a PCB version of the circuit - it automatically replicates the components in all views, you 'just' have to lay them out.

Here is the Fritzing PCB version:  
![vbs-amp PCB layout in Fritzing](fritzing/video-amplifier-pcb.png)

(Having a PCB made by a professional service does not make much sense for a single modification circuit. But I do think the PCB result looks very neat.)

After struggling some time, I was able to made a breadboard version with Fritzing - there were a few very nice people on the Fritzing forums helping me over some stumbling blocks - but it is different from the version I had already plugged together, so I don't show it here. (You can see it for yourself, the Fritzing design file is in the same repository as this text file.)

Here is a photo of my own breadboard version of the circuit, which I used for testing:  
![vbs-amp breadboard version](media/conic-2020-07-27-vbs-amp-breadboard.jpg)

And I tested it with the Conic TG-621:  
![vbs-amp breadboard test with Conic TG-621](media/conic-2020-07-27-vbs-amp-test.jpg)

(On the top of the breadboard is a [Joule Thief circuit](https://en.wikipedia.org/wiki/Joule_thief), not related to the VBS amplifier.)

All right and well, but does it work? It does! After successful testing I now wanted to make a stripboard version (it needs to be built it into the Conic, after all).

A couple of days earlier I had used LibreOffice Impress to make (sort of) a stripboard schematic to help with it:  
![vbs-amp stripboard schematic](impress/vbs-amp.png)

Then I soldered it, using the components from the breadboard:  
![vbs-amp stripboard version](media/conic-2020-07-26-board.jpg)

I re-used the black coax cable that was already in the Conic, but replaced the RF plug at the end with an RCA plug for A/V connections. Unfortunately, I had only a red plug (yellow would be the correct colour), but as the Conic does not have an external audio output, there is no danger of mixing it up with the plug for the right audio channel.

And before soldering it to the Conic, I tested again with the stripboard version - and it still worked:  
![vbs-amp stripboard test](media/conic-2020-07-26-vbs-amp.jpg)

This is how it is connected at the underside of the Conic circuit board (red=+6.2V, blue=GND, yellow=VBS_IN):  
![vbs-amp stripboard connections](media/conic-2020-07-27-vbs-amp-connections.jpg)

And here is the final 'built-in' image before closing the housing:  
![vbs-amp stripboard inside Conic TG-621 TV game](media/conic-2020-07-27-vbs-amp-built-in.jpg)

To pad and isolate the small circuit board from the other parts of the Conic TG-621 device, I used part of an old padded envelope. Not the most professional internal housing, but it does its job, and the amplifier only draws a few milliampere, so it does not get very hot.

### Other notes

One thing I learned during my tests is that the Conic already works from about 6V (tested with a simple stabilized power supply), and this is still enough to output a decent picture.

It will work with higher voltages, too. But the linear power regulator transistor (Q1, ST1702, NPN) has to convert the excess power into heat. When I measured it at 9V, the Conic was drawing about 800mA. If 1/3 of the power (3V of 9V) has to be dissipated, at 0.8A that's around 2.4W wasted as heat. Looking into the data sheet, the ST1702 has a maximum power dissipation of 600mW, so with 9V batteries it is already working out of spec. 

Therefore I will recommend using a 6V or 7.5V power supply (with 1A max. current) for the unit.

`TODO`: Measure the power consumtion again on the device. Looking at the AY-3-8500 data sheet, it should only draw between 40mA (typical) and 60mA (max.). This is quite a way off from 800mA. On the other hand, the AY-3-8500 gets also quite hot, and the example circuits from General Instruments never hard-wire the outputs together. Obviously the circuit works, but hard-wiring the outputs together certainly 'stresses' the IC (several pins driven low and high are connected together) and could well be the cause for this.

Looking at the discrete and un-shielded RF modulator, the discrete L/C based clock generator, and the 'hard-wired' IC outputs on the PCB, this device looks like the manufacturer made an attempt to simplify the design and make it as cheaply to manufacture as possible.

## FIN

That's it. Hope you enjoyed it! It was fun to do, and it is nice to know that this modification will make it possible to let more people play with it, and give the Conic a longer lease of life.

Hagen Patzke, 2020-07-29

----

- 2020-08-06: add bonus material

  Combination from PCB bottom photo and (horizontally mirrored) PCB top photo that I used for tracing the circuit:

  ![Photo of PCB bottom and (mirrored) top used for making the partial schematic](media/conic-pcb.jpg)

`#eof`
