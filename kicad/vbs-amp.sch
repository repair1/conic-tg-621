EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Conic VBS Amplifier"
Date "2020-07-23"
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2350 1600 2200 1600
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 5F19C2B8
P 3450 2550
F 0 "J2" H 3530 2542 50  0000 L CNN
F 1 "Video_OUT" H 3530 2451 50  0000 L CNN
F 2 "" H 3450 2550 50  0001 C CNN
F 3 "~" H 3450 2550 50  0001 C CNN
F 4 "J" H 3450 2550 50  0001 C CNN "Spice_Primitive"
F 5 "Video_OUT" H 3450 2550 50  0001 C CNN "Spice_Model"
F 6 "N" H 3450 2550 50  0001 C CNN "Spice_Netlist_Enabled"
	1    3450 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 5F19CB5D
P 1800 2450
F 0 "C1" H 1918 2496 50  0000 L CNN
F 1 "1uF" H 1918 2405 50  0000 L CNN
F 2 "" H 1838 2300 50  0001 C CNN
F 3 "~" H 1800 2450 50  0001 C CNN
	1    1800 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5F19DF3F
P 2200 1300
F 0 "R4" H 2270 1346 50  0000 L CNN
F 1 "1k" H 2270 1255 50  0000 L CNN
F 2 "" V 2130 1300 50  0001 C CNN
F 3 "~" H 2200 1300 50  0001 C CNN
	1    2200 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5F19F006
P 2650 2100
F 0 "R5" H 2720 2146 50  0000 L CNN
F 1 "6800" H 2720 2055 50  0000 L CNN
F 2 "" V 2580 2100 50  0001 C CNN
F 3 "~" H 2650 2100 50  0001 C CNN
	1    2650 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5F19F5D7
P 2650 2950
F 0 "R6" H 2720 2996 50  0000 L CNN
F 1 "110" H 2720 2905 50  0000 L CNN
F 2 "" V 2580 2950 50  0001 C CNN
F 3 "~" H 2650 2950 50  0001 C CNN
	1    2650 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 1450 2200 1600
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5F19AC2F
P 1400 2700
F 0 "J1" H 1318 2917 50  0000 C CNN
F 1 "Video_IN" H 1318 2826 50  0000 C CNN
F 2 "" H 1400 2700 50  0001 C CNN
F 3 "~" H 1400 2700 50  0001 C CNN
F 4 "J" H 1400 2700 50  0001 C CNN "Spice_Primitive"
F 5 "Video_IN" H 1400 2700 50  0001 C CNN "Spice_Model"
F 6 "N" H 1400 2700 50  0001 C CNN "Spice_Netlist_Enabled"
	1    1400 2700
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5F19D4A0
P 1050 1350
F 0 "R2" H 1120 1396 50  0000 L CNN
F 1 "4700" H 1120 1305 50  0000 L CNN
F 2 "" V 980 1350 50  0001 C CNN
F 3 "~" H 1050 1350 50  0001 C CNN
	1    1050 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 5F1DD746
P 3050 2100
F 0 "C2" H 3168 2146 50  0000 L CNN
F 1 "1uF" H 3168 2055 50  0000 L CNN
F 2 "" H 3088 1950 50  0001 C CNN
F 3 "~" H 3050 2100 50  0001 C CNN
F 4 "C" H 3050 2100 50  0001 C CNN "Spice_Primitive"
F 5 "1u" H 3050 2100 50  0001 C CNN "Spice_Model"
F 6 "Y" H 3050 2100 50  0001 C CNN "Spice_Netlist_Enabled"
	1    3050 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 1800 2650 1850
Wire Wire Line
	3050 1850 2650 1850
Connection ~ 2650 1850
Wire Wire Line
	2650 1850 2650 1950
Wire Wire Line
	3250 2650 3250 3250
Wire Wire Line
	2650 3100 2650 3250
Wire Wire Line
	2650 1050 2650 1400
Wire Wire Line
	2200 1150 2200 1050
Connection ~ 2200 1050
Wire Wire Line
	2200 1050 2650 1050
Wire Wire Line
	2650 3250 3050 3250
$Comp
L Diode:1N4148 D1
U 1 1 5F1F03E3
P 1550 2100
F 0 "D1" H 1550 1884 50  0000 C CNN
F 1 "1N4148" H 1550 1975 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 1550 1925 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 1550 2100 50  0001 C CNN
F 4 "D" H 1550 2100 50  0001 C CNN "Spice_Primitive"
F 5 "1N4148" H 1550 2100 50  0001 C CNN "Spice_Model"
F 6 "Y" H 1550 2100 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "component.lib" H 1550 2100 50  0001 C CNN "Spice_Lib_File"
F 8 "2 1" H 1550 2100 50  0001 C CNN "Spice_Node_Sequence"
	1    1550 2100
	-1   0    0    1   
$EndComp
$Comp
L Device:R R7
U 1 1 5F1F6C9D
P 3050 2950
F 0 "R7" H 3120 2996 50  0000 L CNN
F 1 "75" H 3120 2905 50  0000 L CNN
F 2 "" V 2980 2950 50  0001 C CNN
F 3 "~" H 3050 2950 50  0001 C CNN
F 4 "R" H 3050 2950 50  0001 C CNN "Spice_Primitive"
F 5 "Y" H 3050 2950 50  0001 C CNN "Spice_Netlist_Enabled"
F 6 "75" H 3050 2950 50  0001 C CNN "Spice_Model"
	1    3050 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 3100 3050 3250
Connection ~ 3050 3250
Wire Wire Line
	3050 3250 3250 3250
Wire Wire Line
	1600 2700 1800 2700
Wire Wire Line
	1800 2800 1800 2700
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5F227DD7
P 4400 1050
F 0 "J3" H 4480 1042 50  0000 L CNN
F 1 "Power" H 4480 951 50  0000 L CNN
F 2 "" H 4400 1050 50  0001 C CNN
F 3 "~" H 4400 1050 50  0001 C CNN
F 4 "J" H 4400 1050 50  0001 C CNN "Spice_Primitive"
F 5 "Power" H 4400 1050 50  0001 C CNN "Spice_Model"
F 6 "N" H 4400 1050 50  0001 C CNN "Spice_Netlist_Enabled"
	1    4400 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 1050 3800 1050
Connection ~ 2650 1050
Wire Wire Line
	4200 1150 4200 3250
Wire Wire Line
	4200 3250 3800 3250
Connection ~ 3250 3250
Wire Wire Line
	3050 2550 3250 2550
Wire Wire Line
	3050 1850 3050 1950
Wire Wire Line
	3050 2250 3050 2550
Wire Wire Line
	3050 2800 3050 2550
Connection ~ 3050 2550
Wire Wire Line
	1800 2600 1800 2700
Connection ~ 1800 2700
Wire Wire Line
	1800 2100 1900 2100
Wire Wire Line
	1800 2300 1800 2100
Wire Wire Line
	1700 2100 1800 2100
Connection ~ 1800 2100
$Comp
L Device:R R1
U 1 1 5F19FADF
P 1800 2950
F 0 "R1" H 1870 2996 50  0000 L CNN
F 1 "75" H 1870 2905 50  0000 L CNN
F 2 "" V 1730 2950 50  0001 C CNN
F 3 "~" H 1800 2950 50  0001 C CNN
F 4 "R" H 1800 2950 50  0001 C CNN "Spice_Primitive"
F 5 "75" H 1800 2950 50  0001 C CNN "Spice_Model"
F 6 "Y" H 1800 2950 50  0001 C CNN "Spice_Netlist_Enabled"
	1    1800 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5F19F7E7
P 1050 2950
F 0 "R3" H 1120 2996 50  0000 L CNN
F 1 "2000" H 1120 2905 50  0000 L CNN
F 2 "" V 980 2950 50  0001 C CNN
F 3 "~" H 1050 2950 50  0001 C CNN
	1    1050 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 1500 1050 2100
Wire Wire Line
	2650 2250 2650 2550
Wire Wire Line
	2200 1900 2200 1600
Connection ~ 2200 1600
Wire Wire Line
	1050 1200 1050 1050
Wire Wire Line
	1050 1050 2200 1050
Wire Wire Line
	1050 3100 1050 3250
Wire Wire Line
	1050 3250 1600 3250
Connection ~ 2650 3250
Wire Wire Line
	1800 3100 1800 3250
Connection ~ 1800 3250
Wire Wire Line
	1800 3250 2150 3250
Wire Wire Line
	1400 2100 1050 2100
Connection ~ 1050 2100
Wire Wire Line
	1050 2100 1050 2800
Wire Wire Line
	2200 2300 2200 2550
Wire Wire Line
	2200 2550 2650 2550
Connection ~ 2650 2550
Wire Wire Line
	2650 2550 2650 2800
Wire Wire Line
	1600 2800 1600 3250
Connection ~ 1600 3250
Wire Wire Line
	1600 3250 1800 3250
Wire Wire Line
	3800 1350 3800 1050
Connection ~ 3800 1050
Wire Wire Line
	3800 1050 4200 1050
Wire Wire Line
	3800 1950 3800 3250
Connection ~ 3800 3250
Wire Wire Line
	3800 3250 3250 3250
Wire Wire Line
	2150 2800 2150 2700
Wire Wire Line
	2150 2700 1800 2700
Wire Wire Line
	2150 3200 2150 3250
Connection ~ 2150 3250
Wire Wire Line
	2150 3250 2650 3250
Text Notes 1050 850  0    50   ~ 0
.tran 2u 20m
$Comp
L pspice:VSOURCE Vcc
U 1 1 5F2AC768
P 3800 1650
F 0 "Vcc" H 4028 1696 50  0000 L CNN
F 1 "6.2" H 4028 1605 50  0000 L CNN
F 2 "" H 3800 1650 50  0001 C CNN
F 3 "~" H 3800 1650 50  0001 C CNN
F 4 "V" H 3800 1650 50  0001 C CNN "Spice_Primitive"
F 5 "Y" H 3800 1650 50  0001 C CNN "Spice_Netlist_Enabled"
F 6 "dc 6.2" H 3800 1650 50  0001 C CNN "Spice_Model"
	1    3800 1650
	1    0    0    -1  
$EndComp
$Comp
L Simulation_SPICE:VSIN Vvid
U 1 1 5F2E3377
P 2150 3000
F 0 "Vvid" H 2280 3091 50  0000 L CNN
F 1 "VSIN" H 2280 3000 50  0000 L CNN
F 2 "" H 2150 3000 50  0001 C CNN
F 3 "~" H 2150 3000 50  0001 C CNN
F 4 "Y" H 2150 3000 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 2150 3000 50  0001 L CNN "Spice_Primitive"
F 6 "sin(174m 174m 16125)" H 2280 2909 50  0000 L CNN "Spice_Model"
	1    2150 3000
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC107 Q1
U 1 1 5F1A1F4C
P 2100 2100
F 0 "Q1" H 2291 2146 50  0000 L CNN
F 1 "BC107B" H 2291 2055 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-18-3" H 2300 2025 50  0001 L CIN
F 3 "http://www.b-kainka.de/Daten/Transistor/BC108.pdf" H 2100 2100 50  0001 L CNN
F 4 "Q" H 2100 2100 50  0001 C CNN "Spice_Primitive"
F 5 "BC107B" H 2100 2100 50  0001 C CNN "Spice_Model"
F 6 "Y" H 2100 2100 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "3 2 1" H 2100 2100 50  0001 C CNN "Spice_Node_Sequence"
F 8 "component.lib" H 2100 2100 50  0001 C CNN "Spice_Lib_File"
	1    2100 2100
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:2N3904 Q2
U 1 1 5F1A327B
P 2550 1600
F 0 "Q2" H 2740 1554 50  0000 L CNN
F 1 "2N3904" H 2740 1645 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 2750 1525 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 2550 1600 50  0001 L CNN
F 4 "Q" H 2550 1600 50  0001 C CNN "Spice_Primitive"
F 5 "2N3906" H 2550 1600 50  0001 C CNN "Spice_Model"
F 6 "Y" H 2550 1600 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "3 2 1" H 2550 1600 50  0001 C CNN "Spice_Node_Sequence"
F 8 "component.lib" H 2550 1600 50  0001 C CNN "Spice_Lib_File"
	1    2550 1600
	1    0    0    1   
$EndComp
$EndSCHEMATC
